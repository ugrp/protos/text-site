class PageContent extends HTMLElement {
	get name() {
		return this.getAttribute("name") || "index";
	}
	get ext() {
		return this.getAttribute("ext") || "txt";
	}
	get pageContentUrl() {
		return `./content/${this.name}.${this.ext}`;
	}
	get $template() {
		return this.querySelector("template");
	}
	async connectedCallback() {
		const [frontMatter, body] = await this.fetchPageData();
		this.frontMatter = frontMatter;
		this.body = body;
		this.render();
	}
	async fetchPageData() {
		const data = await fetch(this.pageContentUrl).then((res) => {
			return res.text();
		});
		const [attributes, body] = data.split("\n---\n");
		if (body) {
			const url = new URL(`${window.location}/?${attributes}`);
			if (url) return [url.searchParams, body];
		} else {
			/* the attributes are the body */
			return [null, attributes];
		}
	}
	render() {
		const $dom = this.$template.content.cloneNode("true");
		$dom.querySelectorAll("placeholder").forEach(($placeholder) => {
			const placeholderName = $placeholder.getAttribute("name");
			const placeholderVal =
				placeholderName === "body"
					? this.body
					: this.frontMatter.get(placeholderName);
			const $placeholderValue = document.createTextNode(placeholderVal);
			console.log($placeholderValue);
			$placeholder.parentNode.replaceChild($placeholderValue, $placeholder);
		});
		this.parentNode.replaceChild($dom, this);
	}
}

window.customElements.define("page-content", PageContent);
