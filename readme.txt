# text-site

This is a website made of content found in text files, used with a
"light description system" based on URL Search Params and HTML Element
DOM attributes.

It tries to follow and implement the uip specification for content
organisation https://gitlab.com/uip123/uip123.gitlab.io.
