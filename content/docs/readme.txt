# Development
This project has no build tool and could use any HTTP server, for
practicity run `npm run dev` in the current folder.

## Text format
The format for the `.txt` files in the `./content`folder, are similar
to Markdown's frontmatter, and can be used to store "file attributes",
which can be followed by one delimiter `---` that symbolizes the
beginning of the file's content.

```text
&title=Hello
&my-number=23
---
The content of the file
```

If no delimiter is present, the entire file is considered as the
content.

Unlike markdown, only one delimiter is required, after the frontmatter.

## web-components
In the `./src` folder is a `PageContent` javascript web-component,
that expects a children HTML `<template>` element, and will use it to
replace itself as the template of the page.

It will look for all HTML `<placeholder/>` elements, with a `name`
attribute, and use its value to replace the content from the text
files frontmatter.

The content of the files (not the frontmatter), can be accessed with a
placeholder of name `body`.

```html
<page-content name="about">
	<template>
		<h1>
			<placeholder name="title" />
		</h1>
		<div>
			<placeholder name="body" />
		</div>
		<a href="https://example.org">Example link</a>
	</template>
</page-content>
```

# Objectives
Try out "vanilla" web deveopment of sites and pages, with clear
speration of concerns between data/templates/web-components/styles.

Explore no-dependency development, and web-native file formats.

Research on the data format:
- text files
- attributes in front matter, json, multiline
- support links, media, images/videos, other html elements, lists
  (blockquote, details…)
- parsable by URL or other mean
